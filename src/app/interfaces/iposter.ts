export interface IPoster {
    posterId: number,
    name: string,
    content: any
}