import { Ieventid } from "./ieventid";
import { Iuserid } from "./iuserid";

export interface iTicket {
    ticketId: number,
    isPaid: boolean,
    paidDate: Date,
    quantity: number,
    ticketEvent: Ieventid,
    ticketUser: Iuserid
}
