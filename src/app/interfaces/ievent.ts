import { IPoster } from "./iposter";
import { IUser } from "./iuser";

export interface IEvent {
    eventId: number,
    title: string,
    description: string,
    creationDate: Date,
    startDate: Date,
    endDate: Date,
    registrationEndDate: Date,
    eventStatus: string,
    location: string,
    nbrOfPlaces: number,
    price:number,
    user: IUser,
    poster: IPoster
}
