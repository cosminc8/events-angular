export interface IUser {
    userId:number,
    username: string,
    email: string
    password: string,
    description: string,
    isActive: boolean,
    roleList: string[]
}
