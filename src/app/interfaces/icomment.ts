import { IEvent } from "./ievent";
import { IUser } from "./iuser";

export interface IComment {
    commentId: number,
    content: string,
    creationDate: Date,
    commentEvent: IEvent,
    commentUser: IUser
    
}