export interface Iitem {
    quantity: number,
    price:number,
    eventId: number,
    eventTitle: string,
    userId: number
}
