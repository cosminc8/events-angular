import { Ieventid } from "./ieventid";
import { Iuserid } from "./iuserid";

export interface IRegistration {
    registrationId: number,
    registrationDate: Date,
    event: Ieventid,
    user: Iuserid
}
