import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventComponent } from './components/event/event.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { NewEventComponent } from './components/event-new/new-event.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventDetailComponent } from './components/event-detail/event-detail.component';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { PosterComponent } from './components/poster/poster.component';
import { EventEditComponent } from './components/event-edit/event-edit.component';
import { FooterComponent } from './components/footer/footer.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CommentComponent } from './components/comment/comment.component';
import { CartComponent } from './components/cart/cart.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { authInterceptorProviders } from './helpers/auth.interceptor';
import { EventsAdminComponent } from './components/events-admin/events-admin.component';
import { EventsHostComponent } from './components/events-host/events-host.component';
import { EventsUserComponent } from './components/events-user/events-user.component';

@NgModule({
  declarations: [
    AppComponent,
    EventComponent,
    HeaderComponent,
    HomeComponent,
    NewEventComponent,
    EventDetailComponent,
    PosterComponent,
    EventEditComponent,
    FooterComponent,
    CommentComponent,
    CartComponent,
    LoginComponent,
    RegisterComponent,
    EventsAdminComponent,
    EventsHostComponent,
    EventsUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
