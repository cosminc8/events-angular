import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventDetailComponent } from './components/event-detail/event-detail.component';
import { EventComponent } from './components/event/event.component';
import { HomeComponent } from './components/home/home.component';
import { NewEventComponent } from './components/event-new/new-event.component';
import { EventEditComponent } from './components/event-edit/event-edit.component';
import { CartComponent } from './components/cart/cart.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { EventsAdminComponent } from './components/events-admin/events-admin.component';
import { EventsHostComponent } from './components/events-host/events-host.component';
import { EventsUserComponent } from './components/events-user/events-user.component';

const routes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'events', component: EventComponent },
  { path: 'eventsAdmin', component: EventsAdminComponent },
  { path: 'eventsHost', component: EventsHostComponent },
  { path: 'eventsUser', component: EventsUserComponent },
  { path: 'home', component: HomeComponent },
  { path: 'newEvent', component: NewEventComponent },
  { path: 'eventDetail/:id', component: EventDetailComponent },
  { path: 'eventEdit/:id', component: EventEditComponent },
  { path: 'cart' , component: CartComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
