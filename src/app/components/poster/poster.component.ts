import { Component, OnInit } from '@angular/core';
import { PosterService } from 'src/app/services/poster.service';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-poster',
  templateUrl: './poster.component.html',
  styleUrls: ['./poster.component.scss']
})
export class PosterComponent implements OnInit {

  constructor(private posterService: PosterService) { }

  selectedFile!: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message!: string;
  imageName: any;
  imageId!: number;
  @Output() newPosterIdEvent = new EventEmitter<number>();

  ngOnInit(): void {
  }

  //Gets called when the user selects an image
  public onFileChanged(event: any) {
    //Select File
    this.selectedFile = event.target.files[0];
  }
  // Send PostId to parent
  addNewPostId(value: number) {
    this.newPosterIdEvent.emit(value);
  }
  //Gets called when the user clicks on submit to upload the image
  onUpload() {
    console.log("selected file: " + this.selectedFile);
    //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile);
    //Make a call to the Spring Boot Application to save the image
    this.posterService.addPoster(uploadImageData)
      .subscribe((response) => {
        this.imageId = response.posterId;
        this.message = "poster was uploaded";
        console.log("PosterId returned is: " + this.imageId);
        this.addNewPostId(this.imageId);
        this.base64Data = response.content;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        this.imageName = response.name;
      }
      );
  }

  getImage() {
    this.posterService.getPosterById(this.imageId)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.content;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
          this.imageName = this.retrieveResonse.name;
        }
      );
  }
}
