import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IComment } from 'src/app/interfaces/icomment';
import { Ieventid } from 'src/app/interfaces/ieventid';
import { Iuserid } from 'src/app/interfaces/iuserid';
import { CommentService } from 'src/app/services/comment.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  isLoggedIn = false;
  messageValidator: string = "";
  commentForm!: FormGroup;
  cDate: Date = new Date();
  eventWithId: Ieventid ={eventId:0};
  user!: Iuserid;
  userId!:number;
  comments: IComment[] = [];
  nbrOfComments:number = 0;

  @Input() eventId = 0;

  constructor(
    private commentService: CommentService,
    private tokenStorageService: TokenStorageService,
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn){
      const user = this.tokenStorageService.getUser();
      this.userId = user.id;
    }
    this.eventWithId.eventId = this.eventId;
    this.newCommentForm();
    this.loadCommentsForEvent(this.eventId);
  }

  newCommentForm() {
    this.commentForm = new FormGroup({
      content: new FormControl("", [Validators.required]),
      creationDate: new FormControl("", [Validators.required]),
      commentEvent: new FormControl("", [Validators.required]),
      commentUser: new FormControl("", [Validators.required]),
    });
  }

  submitComment() {
    this.commentForm.patchValue({
      creationDate: this.cDate,
      commentUser: {userId : this.userId},
      commentEvent: this.eventWithId
    });
    if (this.commentForm.valid) {
      this.commentService.addComment(this.commentForm.value).subscribe(
        data => {
          console.log("sent comment: " + JSON.stringify(this.commentForm.value));
          this.commentForm.reset();
          this.messageValidator="";
          this.loadCommentsForEvent(this.eventId);
        },
        err => console.log(err)
      )
    } else {
      this.messageValidator = "Comment can't be empty.";
      console.log(this.commentForm.value);
    }
  }

  loadCommentsForEvent(id: number){
    return this.commentService.getCommentsByEventId(id).subscribe(
      data =>{
        this.comments = data; 
        this.nbrOfComments = data.length;
      },
      err => console.log(err)
    )
  }
}
