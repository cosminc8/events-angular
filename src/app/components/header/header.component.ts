import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private roles!: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showHostBoard = false;
  username!: string;

  constructor(private tokenStorageService: TokenStorageService) {}

  ngOnInit(): void {
      this.isLoggedIn = !!this.tokenStorageService.getToken();

      if(this.isLoggedIn) {
        const user = this.tokenStorageService.getUser();
        this.roles = user.roles;

        this.showAdminBoard = this.roles.includes('ADMIN');
        this.showHostBoard = this.roles.includes('HOST');

        this.username = user.username;
      }
  }

  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }

}
