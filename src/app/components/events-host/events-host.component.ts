import { Component, OnInit } from '@angular/core';
import { IEvent } from 'src/app/interfaces/ievent';
import { EventService } from 'src/app/services/event.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-events-host',
  templateUrl: './events-host.component.html',
  styleUrls: ['./events-host.component.scss']
})
export class EventsHostComponent implements OnInit {

  events: IEvent[] = [];
  userId!: number;
  isLoggedIn = false;

  constructor(private eventService: EventService, private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.userId = user.id;
    }
    this.loadEvents(this.userId);
  }

  loadEvents(userId: number) {
    return this.eventService.getEventByUserId(userId).subscribe(
      data => {
        this.events = data;
      },
      err => console.log(err)
    )
  }

  removeEvent(id: number) {
    this.eventService.deleteEvent(id).subscribe(
      data => {
        window.location.reload();
        console.log("event with " + id + " was deleted");
      }
    )
  }

  searchEventByTitle(title: string) {
    if (title == "") {
      this.loadEvents(this.userId);
    } else {
      this.eventService.getEventByTitle(title).subscribe(
        data => {
          this.events = data;
        },
        err => console.log(err)
      )
    }

  }

}
