import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsHostComponent } from './events-host.component';

describe('EventsHostComponent', () => {
  let component: EventsHostComponent;
  let fixture: ComponentFixture<EventsHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventsHostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
