import { Component, OnInit } from '@angular/core';
import { Iitem } from 'src/app/interfaces/iitem';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  tickets: any[] = [];
  items: Iitem[] = [];
  message: string = "";

  constructor(
    private cartService: CartService,
    ) { }

  ngOnInit(): void {
    this.items = this.cartService.getTickets();
    if(this.items.length == 0){
      this.message="empty cart";
    }
  }

  removeItem(item:Iitem){
    var index = this.items.indexOf(item);
    this.items.splice(index, 1);
  }

  convertItemToTicket(item: Iitem){
    var ticket: any={
    isPaid:true,
    paidDate: new Date(),
    quantity: item.quantity,
    ticketEvent:{eventId: item.eventId},
    ticketUser: {userId: item.userId}
  }
    this.tickets.push(ticket);
  }

  onSubmit(): void {
    //foreach item subscribe new ticket in db
    this.items.forEach(elem => this.convertItemToTicket(elem));
    console.log("items in cart:" + JSON.stringify(this.items));
    this.tickets.forEach(elem => this.cartService.saveTicket(elem).subscribe(
      data => {},
      err => console.log(err)
    ));
    console.log("tickets in cart:" + JSON.stringify(this.tickets));
    this.items = this.cartService.clearCart();
    window.alert("your order has been placed");
  }
}
