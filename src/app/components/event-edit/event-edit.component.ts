import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IEvent } from 'src/app/interfaces/ievent';
import { IPoster } from 'src/app/interfaces/iposter';
import { EventService } from 'src/app/services/event.service';
import { PosterService } from 'src/app/services/poster.service';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.scss']
})
export class EventEditComponent implements OnInit {

  event!: IEvent;
  selectedId: any;
  eventForm!: FormGroup;
  messageValidator: string = "";
  oldPosterIdFromChild!: number;
  poster!: IPoster;
  newPoster: boolean = false;
  showPosterImg: boolean = true;
  retrievedImage: any;
  base64Data: any;

  constructor(
    private eventService: EventService,
    private posterService: PosterService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.selectedId = params.get('id');
      console.log("selectedId for details is: " + this.selectedId);
    });
    this.loadEvent(this.selectedId);
    this.newEventForm();
  }

  loadEvent(id: number) {
    return this.eventService.getEventById(id).subscribe(
      data => {
        this.event = data;
        this.poster = data.poster;
        this.base64Data = data.poster.content;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        console.log("Event loaded: " + JSON.stringify(this.event));
        this.eventForm.patchValue(data);
      },
      err => console.log(err)
    )
  }

  newEventForm() {
    this.eventForm = new FormGroup({
      eventId: new FormControl("", [Validators.required]),
      poster: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      creationDate: new FormControl("", [Validators.required]),
      startDate: new FormControl("", [Validators.required]),
      endDate: new FormControl("", [Validators.required]),
      registrationEndDate: new FormControl("", [Validators.required]),
      eventStatus: new FormControl("", [Validators.required]),
      location: new FormControl("", [Validators.required]),
      nbrOfPlaces: new FormControl("", [Validators.required]),
      price: new FormControl("", [Validators.required]),
      user: new FormControl("", [Validators.required]),
    });
  }

  setPosterId(id:number){
    this.oldPosterIdFromChild=this.poster.posterId;
    console.log("received from child PosterId: " + this.oldPosterIdFromChild);
    this.showPosterImg = false;
    this.posterService.getPosterById(id).subscribe(
      data =>{
        this.poster = data;
        this.newPoster = true;
      },
      err => console.log(err)
    )
  }

  updateEvent() {
    this.eventForm.patchValue({
      eventId: this.event.eventId,
      poster: this.poster
    });
    if (this.eventForm.valid) {
      this.eventService.updateEvent(this.eventForm.value).subscribe(
        data => {
          this.eventForm.reset;
          this.messageValidator = "Event was updated";
          this.router.navigate(['/events']);
          //console.log("Sent updated event: " + JSON.stringify(this.eventForm.value));
        },
        err => console.log(err)
      )
    } else {
      this.messageValidator = "Please fill out the form before submiting.";
    }
    if(this.newPoster){
      this.posterService.deletePoster(this.oldPosterIdFromChild).subscribe(
        data =>{
          console.log("old Poster was deleted:" + data.content)
        },
        err => console.log(err)
      )
    }
  }

}
