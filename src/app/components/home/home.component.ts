import { Component, OnInit } from '@angular/core';
import { IEvent } from 'src/app/interfaces/ievent';
import { IPoster } from 'src/app/interfaces/iposter';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  events: IEvent[] = [];
  event!: IEvent;
  poster!: IPoster;
  image: any;
  retrivedImages: any[] = [];
  base64Data: any;
  dates: any[] = [];

  constructor(private eventService: EventService) { }

  ngOnInit(): void {
    this.loadEvents();
  }

  loadEvents() {
    return this.eventService.getEventsForHomePage().subscribe(
      data => {
        this.events = data;
        console.log("HomePage events loaded: " + JSON.stringify(this.events));
        this.getImage();
        this.formatDate();
      },
      err => console.log(err)
    )
  }

  getImage() {
    this.events.forEach(ev => {
      this.base64Data = ev.poster.content;
      this.image = 'data:image/jpeg;base64,' + this.base64Data;
      this.retrivedImages.push(this.image);
    });
  }

  showImg(index: number): any {
    return this.retrivedImages[index];
  }

  formatDate() {
    this.events.forEach(ev => {
      const d = ev.startDate;
      let text = d.toLocaleString();
      const words = text.split("T");
      const result = "Starts on: " + words[0] + " hour: " + words[1];
      this.dates.push(result);
    });
  }

  showDate(index: number): any {
    return this.dates[index];
  }

  
}
