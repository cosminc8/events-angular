import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IPoster } from 'src/app/interfaces/iposter';
import { Iuserid } from 'src/app/interfaces/iuserid';
//import { IUser } from 'src/app/interfaces/iuser';
import { EventService } from 'src/app/services/event.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.component.html',
  styleUrls: ['./new-event.component.scss']
})
export class NewEventComponent implements OnInit {

  eventForm!: FormGroup;
  messageValidator: string = "";
  cDate: Date = new Date();
  user!: Iuserid;
  posterIdFromChild!: number;
  poster!: IPoster;
  userId!: number;
  isLoggedIn = false;

  constructor(private eventService: EventService,
    private router: Router,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.userId = user.id;
    }
    this.newEventForm();
  }

  newEventForm() {
    this.eventForm = new FormGroup({
      poster: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      creationDate: new FormControl("", [Validators.required]),
      startDate: new FormControl("", [Validators.required]),
      endDate: new FormControl("", [Validators.required]),
      registrationEndDate: new FormControl("", [Validators.required]),
      eventStatus: new FormControl("", [Validators.required]),
      location: new FormControl("", [Validators.required]),
      nbrOfPlaces: new FormControl("", [Validators.required]),
      price: new FormControl("", [Validators.required]),
      user: new FormControl("", [Validators.required]),
    });
  }

  setPosterId(id: number) {
    this.posterIdFromChild = id;
    console.log("received from child PosterId: " + this.posterIdFromChild);
    this.poster = { posterId: this.posterIdFromChild, name: "", content: "" };
  }

  submitEvent() {
    this.eventForm.patchValue({
      poster: this.poster,
      creationDate: this.cDate,
      user:{userId : this.userId} 
    });
    if (this.eventForm.valid) {
      this.eventService.addEvent(this.eventForm.value).subscribe(
        data => {
          this.eventForm.reset;
          this.messageValidator = "NewEvent form has been submited.";
          this.router.navigate(['/events']);
        },
        err => console.log(err)
      )
    } else {
      this.messageValidator = "Please fill out the form before submiting.";
      console.log(this.eventForm.value);
    }
  }
}
