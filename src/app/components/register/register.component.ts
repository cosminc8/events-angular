import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: any = {};
  roles: string[]=[];
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  hasGuest = false;
  hasHost = false; 

  constructor( private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if(this.hasGuest){
      this.roles.push("guest");
    }
    if(this.hasHost){
      this.roles.push("host");
    }
    this.form.role=this.roles;
    this.authService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
