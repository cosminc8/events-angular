import { Component, OnInit } from '@angular/core';
import { IEvent } from 'src/app/interfaces/ievent';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  events: IEvent[] = [];

  constructor(private eventService: EventService) { }

  ngOnInit(): void {
    this.loadAllEvents();
  }

  loadAllEvents(){
    return this.eventService.getAllEvents().subscribe(
      data => {
        this.events = data;
        console.log("Events loaded: " + JSON.stringify(this.events));
      },
      err => console.log(err)
    )
  }

  removeEvent(id:number){
    this.eventService.deleteEvent(id).subscribe(
      data => {
        window.location.reload();
        console.log("event with " + id + " was deleted");
      }
    )
  }

  searchEventByTitle(title: string){
    if(title == ""){
      this.loadAllEvents();
    } else{
      this.eventService.getEventByTitle(title).subscribe(
        data =>{
          this.events = data;
          console.log("Events loaded: " + JSON.stringify(this.events));
        },
        err => console.log(err)
      )
    }
    
  }

}
