import { Component, Input, OnInit } from '@angular/core';
import { IEvent } from 'src/app/interfaces/ievent';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { EventService } from 'src/app/services/event.service';
import { IPoster } from 'src/app/interfaces/iposter';
import { CartService } from 'src/app/services/cart.service';
import { Ieventid } from 'src/app/interfaces/ieventid';
import { Iitem } from 'src/app/interfaces/iitem';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  isLoggedIn = false;
  userId!: number;
  event!: IEvent;
  eventWithId!: Ieventid;
  selectedId: any;
  poster!: IPoster;
  retrievedImage: any;
  base64Data: any;
  cartItem: Iitem = {
    quantity: 0,
    price: 0,
    eventId: 0,
    eventTitle: "",
    userId: 0
  };
  isPaidEvent: boolean = false;
  availablePlaces: number = 0;
  currentDate!: Date;
  isUserRegisteredToEvent: boolean = false;
  registrationId!:number;
  isTicketInCart: boolean = false;

  constructor(
    private eventService: EventService,
    private cartService: CartService,
    private route: ActivatedRoute,
    private tokenStorageService: TokenStorageService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.selectedId = params.get('id');
      console.log("Event Detail - selectedId is: " + this.selectedId);
    });
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.userId = user.id;
    }
    this.eventWithId={eventId: this.selectedId}
    this.loadEvent(this.selectedId);
    this.checkRegistration(this.userId, this.selectedId);
    this.checkTicketInCart(this.selectedId);
  }

  loadEvent(id: number) {
    return this.eventService.getEventById(id).subscribe(
      data => {
        this.event = data;
        this.poster = data.poster;
        this.base64Data = data.poster.content;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        console.log("Event Detail - Event loaded: " + JSON.stringify(this.event));
        this.isPaidEvent = data.price != 0;
        this.availablePlaces = (data.nbrOfPlaces == 0) ? Number.MAX_SAFE_INTEGER : data.nbrOfPlaces;
      },
      err => console.log(err)
    )
  }

  checkRegistration(userId: number, eventId: number) {
    return this.cartService.getRegistrationByUserAndEvent(userId, eventId).subscribe(
      data => {
        console.log("Event details:received registration: " + JSON.stringify(data));
        if (data != null ) {
          this.isUserRegisteredToEvent = true;
          this.registrationId = data.registrationId;
        }
      },
      err => console.log(err)
    )
  }

  registerToEvent() {
    this.currentDate = new Date();
    this.cartService.registerToEvent(
      {
        registrationDate: this.currentDate,
        registrationEvent: this.eventWithId,
        registrationUser: { userId: this.userId }
      }
    ).subscribe(
      data => {
        console.log("Event Detail - regiseter:" + JSON.stringify(data));
        window.location.reload();
      },
      err => console.log(err)
    )
  }

  unregisterFromEvent(){
    window.location.reload();
    this.cartService.unregister(this.registrationId).subscribe(
      data => {
        console.log("Event Detail - UN-regiseter:" + JSON.stringify(data));
      },
      err => console.log(err)
    )
  }

  addToCart(nbrOfTickets: any) {
    this.cartItem = {
      quantity: nbrOfTickets,
      price: nbrOfTickets  * this.event.price,
      eventId: this.selectedId,
      eventTitle: this.event.title,
      userId: this.userId
    }
    this.cartService.addToCart(this.cartItem);
    window.alert("order was added to cart");
  }

  checkTicketInCart(ev: number){
    if(this.cartService.getTickets().find(elem => elem.eventId == ev)){
      this.isTicketInCart = true;
    }
  }

}
