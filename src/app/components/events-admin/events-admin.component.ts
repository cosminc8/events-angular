import { Component, OnInit } from '@angular/core';
import { IEvent } from 'src/app/interfaces/ievent';
import { EventService } from 'src/app/services/event.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-events-admin',
  templateUrl: './events-admin.component.html',
  styleUrls: ['./events-admin.component.scss']
})
export class EventsAdminComponent implements OnInit {
  events: IEvent[] = [];
  userId!: number;
  isLoggedIn = false;

  constructor(private eventService: EventService,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.userId = user.id;
    }
    this.loadAllEvents();
  }

  loadAllEvents() {
    return this.eventService.getAllEvents().subscribe(
      data => {
        this.events = data;
      },
      err => console.log(err)
    )
  }

  removeEvent(id: number) {
    this.eventService.deleteEvent(id).subscribe(
      data => {
        window.location.reload();
        console.log("event with " + id + " was deleted");
      }
    )
  }

  searchEventByTitle(title: string) {
    if (title == "") {
      this.loadAllEvents();
    } else {
      this.eventService.getEventByTitle(title).subscribe(
        data => {
          this.events = data;
        },
        err => console.log(err)
      )
    }

  }

}
