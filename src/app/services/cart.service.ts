import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Iitem } from '../interfaces/iitem';
import { IRegistration } from '../interfaces/iregistration';
import { iTicket } from '../interfaces/iticket';



@Injectable({
  providedIn: 'root'
})
export class CartService {

  httpHeader = {
    headers: new HttpHeaders({"Content-Type": "application/json"})
  }
  private apiUrl = "/server/register";
  private apiUrl2 = "/server/tickets";

  constructor( private http: HttpClient) { }

  items: Iitem[] = [];

  addToCart(item: Iitem) {
    this.items.push(item);
  }

  getTickets(){
    return this.items;
  }

  clearCart(){
    this.items = [];
    return this.items;
  }

  registerToEvent(registration: any): Observable<IRegistration> {
    return this.http.post<IRegistration>(this.apiUrl + "/saveRegistration", JSON.stringify(registration), this.httpHeader);
  }

  getRegistrationByUserAndEvent(userId: number, eventId: number): Observable<IRegistration> {
    return this.http.get<IRegistration>(this.apiUrl + "/allByUserAndEvent/" + userId + "&" + eventId, this.httpHeader);
  }

  unregister(id:number){
    return this.http.delete<IRegistration>(this.apiUrl + "/unregister/" + id, this.httpHeader);
  }

  saveTicket(ticket: any): Observable<iTicket> {
    return this.http.post<iTicket>(this.apiUrl2 + "/buyTicket", JSON.stringify(ticket), this.httpHeader);
  }
}
