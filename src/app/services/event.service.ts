import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { IEvent } from '../interfaces/ievent';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  httpHeader = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  }

  private apiUrl = "/server/events";

  constructor(private http: HttpClient) { }

  getAllEvents(): Observable<IEvent[]> {
    return this.http.get<IEvent[]>(this.apiUrl + "/all");
  }

  getEventsForHomePage(): Observable<IEvent[]> {
    return this.http.get<IEvent[]>(this.apiUrl + "/homePage");
  }

  getEventById(id:number): Observable<IEvent>{
    return this.http.get<IEvent>(this.apiUrl + "/id/" + id, this.httpHeader);
  }

  getEventByTitle(title: string): Observable<IEvent[]> {
    return this.http.get<IEvent[]>(this.apiUrl + "/title/" + title);
  }

  getEventByUserId(id: number): Observable<IEvent[]> {
    return this.http.get<IEvent[]>(this.apiUrl + "/allByUser/" + id, this.httpHeader);
  }

  addEvent(event:any):Observable<IEvent>{
    return this.http.post<IEvent>(this.apiUrl + "/saveEvent", JSON.stringify(event), this.httpHeader);
  }

  deleteEvent(id:number){
    return this.http.delete<IEvent>(this.apiUrl + "/deleteEvent/" + id, this.httpHeader);
  }

  updateEvent(event:any):Observable<IEvent>{
    return this.http.put<IEvent>(this.apiUrl + "/updateEvent", JSON.stringify(event), this.httpHeader);
  }
}
