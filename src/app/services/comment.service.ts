import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { IComment } from "../interfaces/icomment";

@Injectable({
    providedIn: 'root'
})
export class CommentService {
    
    httpHeader = {
        headers: new HttpHeaders({
            "Content-Type": "application/json"
        }) 
    }

    private apiUrl = "/server/comments";

    constructor( private http: HttpClient) {}

    getCommentsByEventId(id:number): Observable<IComment[]> {
        return this.http.get<IComment[]>(this.apiUrl + "/allByEventId/" + id, this.httpHeader);
    }

    addComment(comment:any): Observable<IComment> {
        return this.http.post<IComment>(this.apiUrl + "/saveComment", JSON.stringify(comment), this.httpHeader);
    }

    deleteComment(id:number){
        return this.http.delete<IComment>(this.apiUrl + "/deleteCommentById/" + id, this.httpHeader);
    }

}