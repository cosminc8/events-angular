import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { IPoster } from '../interfaces/iposter';
@Injectable({
    providedIn: 'root'
})
export class PosterService {

    httpHeader = {
        headers: new HttpHeaders({
            "Content-Type": "application/json"
        })
    }

    private apiUrl = "/server/poster/";

    constructor(private http: HttpClient) { }

    getPosterById(id: number): Observable<IPoster> {
        return this.http.get<IPoster>(this.apiUrl + id, this.httpHeader);
    }

    addPoster(poster: any): Observable<IPoster> {
        return this.http.post<IPoster>(this.apiUrl + "savePoster", poster);
    }

    deletePoster(id: number){
        return this.http.delete<IPoster>(this.apiUrl + "/deletePoster/" + id, this.httpHeader);
    }
}
